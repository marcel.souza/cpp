Aula 2 - O tipo padrão `std::vector`
====================================

Nesta segunda aula, veremos um dos tipos mais utilizados da biblioteca padrão,
o tipo `std::vector`. Utilizamos `vector` quando queremos agrupar diversos
elementos de um mesmo tipo. O tipo `vector` é muito utilizado pois sequências
de valores de tamanho variável são extremamente comuns em programação.

Seções
------

1. [O que é um `std::vector` e como criá-lo?](creation.cpp)
2. [Acessando elementos](access.cpp)
3. [Alterando elementos](changing.cpp)
4. [Apêndice 1](appendix1.cpp)


Cuidado especial - `std::vector<bool>`
--------------------------------------

Um aviso: o tipo `std::vector<bool>` não deve ser utilizado. Entenderemos
melhor o que ocorre com ele que o torna uma má ideia, mas por enquanto é
suficiente saber que, diferentemente de qualquer outro `std::vector`,
`std::vector<bool>`, em uma tentativa de otimização de espaço, não se comporta
exatamente como esperado em algumas operações.

Por sorte, `vector<bool>` não é realmente algo de uso muito corriqueiro.  É
bastante raro aparecer necessidade, e existem soluções melhores, como
`std::bitset`.

As leituras complementares 3 e 4 trazem alguns detalhes sobre este assunto. É
interessante lê-las novamente quando já tivermos visto alguns assuntos mais
complexos, como templates.


Leitura complementar
--------------------

1. [`std::vector` - cppreference.com](http://en.cppreference.com/w/cpp/container/vector)
2. [`vector` - C++ Reference](http://www.cplusplus.com/reference/vector/vector/)
3. [On `vector<bool>`- Howard Hinnant : Standard C++](https://isocpp.org/blog/2012/11/on-vectorbool)
4. [17.1.1 Do not use `std::vector<bool>` | High Integrity C++ Coding Standard](http://www.codingstandard.com/rule/17-1-1-do-not-use-stdvector/)
