#include <vector>


int main()
{
    // Apêndice 1
    // ----------

    // Inicializando um vetor da forma "antiga":

    std::vector<int> old;

    old.push_back(1);
    old.push_back(2);
    old.push_back(3);
    old.push_back(4);

    // É isso. Não havia uma forma mais cômoda :(

    return 0;
}
